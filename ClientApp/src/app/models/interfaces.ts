export interface User {
  name: string;
  age: string;
  company: string;
}

export interface Buns {
  productNumber: string;
  type: number;
  date: Date;
  storageLife: number;
  rate: number;
  controlTerm: number;
}

export interface BunTypes {
  nameType: string;
  storageLife: number;
  rate: number;
  controlTerm: number;
  method(aging: number): any;
}
