import { Injectable } from '@angular/core';
import { User, Buns, BunTypes } from './interfaces';

@Injectable()

export class ApiParams {
  user: User = { name: '', age: '', company: '' };

  buns: Buns[] = [
    { productNumber: 'gfr75i', type: 0, date: new Date(2020, 9, 4, 13), storageLife: 50, rate: 4.6, controlTerm: 65 },
    { productNumber: 'ytr643', type: 4, date: new Date(2020, 9, 4, 13), storageLife: 64, rate: 5.6, controlTerm: 3 },
    { productNumber: '5r3h45', type: 1, date: new Date(2020, 9, 4, 15), storageLife: 78, rate: 4.6, controlTerm: 54 },
    { productNumber: 't567fr', type: 3, date: new Date(2020, 9, 4, 14), storageLife: 6, rate: 4.6, controlTerm: 54 },
  ];

  types: BunTypes[] = [
    { nameType: 'круассан', storageLife: 6, rate: 4.7, controlTerm: 3, method: function (aging) { return this.rate - this.rate/50*aging } },
    { nameType: 'крендель', storageLife: 6, rate: 4.7, controlTerm: 3, method: function () { } },
    { nameType: 'багет', storageLife: 6, rate: 4.7, controlTerm: 3, method: function (aging) { return this.rate - this.rate / 50 * aging } },
    { nameType: 'сметанник', storageLife: 6, rate: 4.7, controlTerm: 3, method: function () { } },
    { nameType: 'батон', storageLife: 6, rate: 4.7, controlTerm: 3, method: function (aging) { return this.rate - this.rate / 50 * aging } }
  ];

  croissant(aging) {
    this.types[0].rate - (this.types[0].rate) / 50 * aging;
  }
  pretzel() {
    alert('крендель');
  }
  baguette() {
    alert('багет');
  }
  sourcream() {
    alert('сметанник');
  }
  loaf() {
    alert('батон');
  }

}



