import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiParams } from './ApiParams';
  
@Injectable()
export class HttpService {

  baseUrl: string;
  apiParams: ApiParams;


  constructor(@Inject('BASE_URL') baseUrl: string, private http: HttpClient, api: ApiParams ) {
    this.baseUrl = baseUrl;
    this.apiParams = api;
  }

  getData() {
    return this.http.get(this.baseUrl + 'api/Data/' + this.apiParams.user.name + ',' + this.apiParams.user.age + ',' + this.apiParams.user.company + '/0', {
      responseType: 'blob'
    } );
  }


  setData() {
    const body = {
      name: this.apiParams.user.name,
      age: this.apiParams.user.age,
      company: this.apiParams.user.company
    }

    return this.http.post(this.baseUrl + 'api/Data/', body);
  }



}

