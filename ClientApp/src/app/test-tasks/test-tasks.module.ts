import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Test1Component } from './test1/test1.component';
import { TestTasksRoutingModule } from './test-tasks-routing.module';
import { FetchDataComponent } from './test1/fetch-data/fetch-data.component';

@NgModule({
  declarations: [Test1Component, FetchDataComponent],
  imports: [
    CommonModule,
    TestTasksRoutingModule,
    FormsModule
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    RouterModule
  ]
})

export class TestTasksModule { }
