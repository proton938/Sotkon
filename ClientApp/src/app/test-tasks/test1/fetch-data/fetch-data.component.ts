import { Component, OnInit } from '@angular/core';
import { ApiParams } from '../../../models/ApiParams';
import { HttpService } from '../../../models/http.service';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
  styleUrls: ['./fetch-data.component.css']
})
export class FetchDataComponent implements OnInit {

  apiParams: ApiParams;

  constructor(private httpService: HttpService, api: ApiParams) {
    this.apiParams = api;
  }

  ngOnInit() {
  }




}
