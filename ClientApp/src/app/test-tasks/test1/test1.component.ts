import { Component, OnInit } from '@angular/core';
import { ApiParams } from '../../models/ApiParams';
import { HttpService } from '../../models/http.service';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.css']
})
export class Test1Component implements OnInit {

  apiParams: ApiParams;

  constructor(private httpService: HttpService, api: ApiParams) {
    this.apiParams = api;
  }

  ngOnInit() {
  }

  insertUser(): void {
    if (this.apiParams.user.name != '' && this.apiParams.user.age != '' && this.apiParams.user.company != '') {
      this.httpService.getData()
        .subscribe(
          result => {
            console.log(result);
            alert('Данные удачно сохранены');
            this.apiParams.user.name = '';
            this.apiParams.user.age = '';
            this.apiParams.user.company = '';
          }, error => {
            console.log(error);
            alert('Ошибка сохранения');
          }
        );
    } else {
      alert('Не все данные введены');
    }
  }

}
