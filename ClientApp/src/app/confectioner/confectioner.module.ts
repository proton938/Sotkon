import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BakeryComponent } from './bakery/bakery.component';
import { ConfectionerRoutingModule } from './confectioner-routing.module';


@NgModule({
  declarations: [BakeryComponent],
  imports: [
    CommonModule,
    ConfectionerRoutingModule
  ]
})
export class ConfectionerModule { }
