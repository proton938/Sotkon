import { Component, OnInit } from '@angular/core';
import { ApiParams } from '../../models/ApiParams';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-bakery',
  templateUrl: './bakery.component.html',
  styleUrls: ['./bakery.component.css']
})
export class BakeryComponent implements OnInit {

  datepipe: DatePipe = new DatePipe('en-US')

  apiParams: ApiParams;

  constructor( api: ApiParams ) {
    this.apiParams = api;
  }

  ngOnInit() {
    this.removeDate();
  }

  dateString: any[] = [];

  removeDate() {
    var currentDateTime: any = new Date();
    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));
    var numberDate: any;
    var removeRate: any;
    var aging: any;

    for (let i = 0; i < this.apiParams.buns.length; i++) {

      numberDate = new Date(this.apiParams.buns[i].date);
      removeRate = ((currentDateTime - eraUnix - (numberDate - eraUnix)) / 3600000).toFixed(2);
      aging = this.apiParams.types[i].method(removeRate);

      numberDate = new Date(numberDate - eraUnix + 3600000 * 10);
      numberDate = ((numberDate - eraUnix - (currentDateTime - eraUnix)) / 3600000).toFixed(2);

      this.dateString.push({ startDate: this.datepipe.transform(this.apiParams.buns[0].date, 'yyyy-MM-dd HH:mm:ss'), nextRate: aging, remainsTime: numberDate });
    }
  }


}

interface DateString {
  startDate: string;
}
