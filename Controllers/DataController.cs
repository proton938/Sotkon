﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.IO;

namespace Sotkon.Controllers
{

    class User
    {
        public string Name { get; set; }
        public string Age { get; set; }
        public string Company { get; set; }
    }

    [Route("api/[controller]/{Name}/{Order}")]
    [ApiController]
    public class DataController : ControllerBase
    {

        User user = new User { Name = "", Age = "", Company = "" };

        // GET: api/Data/5
        [HttpGet(Name = "Get", Order = 5)]
        public string Get(string Name, int Order)
        {
            string[] attributes = Name.Split(new char[] { ',' });

            using (FileStream fs = new FileStream("base/user.json", FileMode.Append))
            {
                user = new User() { Name = attributes[0], Age = attributes[1], Company = attributes[2] };
                JsonSerializer.SerializeAsync(fs, user);
                Console.WriteLine("Data has been saved to file");
            }

            return Name + '/' + Order;
        }



        /*
         * 
        // POST: api/Data
        [HttpPost]
        public void Post([FromBody] string value)
        {
            string[] attributes = value.Split(new char[] { ',' });
            using (FileStream fs = new FileStream("base/user.json", FileMode.Append))
            {
                user = new User() { Name = attributes[0], Age = attributes[1], Company = attributes[2] };
                JsonSerializer.SerializeAsync(fs, user);
                Console.WriteLine("Data has been saved to file");
            }
        }


        // PUT: api/Data/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        */

    }
}
